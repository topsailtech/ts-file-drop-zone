<div class="ts_file_drop_zone">
  <icon>cloud_upload</icon>
  <label>
    <span class="instruction">
      <span class="placeholder"></span>
      <span class="drag_instruction">or drag here</span>.
    </span>
    <slot></slot>
  </label>
</div>
