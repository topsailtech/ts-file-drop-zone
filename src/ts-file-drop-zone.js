import WC from 'wc-util'
import { default as template } from './ts-file-drop-zone.tpl'

class TsFileDropZone extends HTMLElement {
  connectedCallback(){
    WC.setup_dom_template(this, template)
      set_placeholder_text.bind(this)(this.placeholder || "Choose a file")
      setup_callbacks.bind(this)()
  }

  static get observedAttributes() { return ["placeholder"] }

  attributeChangedCallback(attr_name,old_val,new_val){
    this.placeholder = new_val
    set_placeholder_text.bind(this)(new_val)
  }
}

function setup_callbacks(){
  ['drag', 'dragstart', 'dragend', 'dragover', 'dragenter', 'dragleave', 'drop'].forEach(
    (evt) => {
      this.addEventListener(
        evt,
        function(e){
          e.preventDefault()
          e.stopPropagation()
        }
      )
    }
  );

  ['dragenter', 'dragover'].forEach(
    (evt) => {
      this.addEventListener(
        evt,
        function(e){
          this.classList.add('is-dragover')
        }
      )
    }
  );

  ['dragleave', 'dragend', 'drop'].forEach(
    (evt) => {
      this.addEventListener(evt, function(e){ this.classList.remove('is-dragover') })
    }
  )

  this.addEventListener('change', (evt)=>{
    let files = evt.target.files
    this.querySelector(".instruction").innerHTML = files.length == 1 ? files[0].name : "" + files.length + " files"
  });

  this.addEventListener('drop', function(e){
    let input = file_input_field.bind(this)()
    if (!input.multiple && input.files.length > 1){
      this.querySelector(".instruction").innerHTML = "Upload one file at a time"
    }
    else {
      input.files = e.dataTransfer.files
      // At least Chrome doesn't trigger a "change" event when setting the input[type=file]'s @files
      input.dispatchEvent(new Event("change", { bubbles: true, cancelable: true }));
    }
  });
}

function file_input_field() {
  return this.querySelector("input[type=file]")
}

function set_placeholder_text(new_text) {
  let label = this.querySelector(".placeholder")
  if (label){
    label.innerText = new_text
  }
}

customElements.define('ts-file-drop-zone', TsFileDropZone)
